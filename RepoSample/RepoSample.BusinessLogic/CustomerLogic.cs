﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.BusinessLogic
{
    public class CustomerLogic
    {
        private readonly DataAccess.Impelement.CustomerRepository _customerRepo;
        public CustomerLogic()
        {
            _customerRepo = new DataAccess.Impelement.CustomerRepository();
        }
        public CustomerLogic(DbContext context)
        {
            _customerRepo = new DataAccess.Impelement.CustomerRepository(context);
        }
        public List<Entity.Customer> GetAll()
        {
            return _customerRepo.GetAll().Select(x => new Entity.Customer() {
            ContactName = x.ContactName,
            CustomerID = x.CustomerID
            }).ToList();
        }
        public void Insert(Entity.Customer entity)
        {
            DataAccess.Customer _customer = new DataAccess.Customer();
            _customer.ContactName = entity.ContactName;
            _customer.CompanyName = entity.CompanyName;
            _customer.CustomerID = entity.ContactName.Substring(0, 3);
            _customerRepo.Insert(_customer);
        }
        public Entity.Customer GetById(string customerId)
        {
            var cus = _customerRepo.GetById(customerId);
            if (cus == null)
                return null;

            return new Entity.Customer() { CustomerID = cus.CustomerID, ContactName = cus.ContactName,CompanyName=cus.CompanyName};
        }
           
    }
}

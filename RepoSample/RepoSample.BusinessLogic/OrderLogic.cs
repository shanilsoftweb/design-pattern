﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.BusinessLogic
{

    public class OrderLogic
    {
        private readonly DataAccess.Impelement.OrderRepository _orderRepo;
        private readonly CustomerLogic _CustomerLogic;
        private readonly DbContext _GenericContext;
        public OrderLogic()
        {
            _GenericContext = new DataAccess.Context.GenericContext();
            _orderRepo = new DataAccess.Impelement.OrderRepository(_GenericContext);
            _CustomerLogic = new CustomerLogic(_GenericContext);
        }
        public IEnumerable<Entity.Order> GetByCustomerId(string customerId)
        {
            var customer = _CustomerLogic.GetById(customerId);
            return _orderRepo.GetOrderBycustomerId(customerId).Select(x => new Entity.Order()
            {
                OrderID = x.OrderID,
                ShipName = x.ShipName,
                Customer = customer
            }).ToList(); ;
        }

    }
}

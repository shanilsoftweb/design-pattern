﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.DataAccess.Interface
{
    public interface IOrderRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetOrderBycustomerId(string id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.DataAccess.Interface
{
    public interface ICustomerRepository<TEntiry>:IRepository<TEntiry> where TEntiry:class
    {
        DataAccess.Customer GetById(string id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.DataAccess.Impelement
{
    public class BaseRepository<TEntity> : Interface.IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext dbContext;
        private bool IsSharedContext = false;
        public BaseRepository()
        {
            dbContext = new NorthwindEntities();
        }
        public BaseRepository(DbContext context)
        {
            dbContext = context;
            IsSharedContext = true;
        }
        public void Insert(TEntity entity)
        {
            dbContext.Set<TEntity>().Add(entity);
            if(!IsSharedContext)
            dbContext.SaveChanges();
        }
        public void Delete(TEntity entity)
        {
            dbContext.Set<TEntity>().Remove(entity);
            if (!IsSharedContext)
            dbContext.SaveChanges();
        }
        public IEnumerable<TEntity> GetAll()
        {
            return dbContext.Set<TEntity>().ToList();
        }
        
    }
}

﻿using RepoSample.DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.DataAccess.Impelement
{
    public class CustomerRepository : BaseRepository<DataAccess.Customer>, ICustomerRepository<DataAccess.Customer>
    {
        private DbContext _dbContext;
        public CustomerRepository()
            : base()
        {
            _dbContext = base.dbContext;
        }
        public CustomerRepository(DbContext dbContext)
            : base(dbContext)
        {
            _dbContext = dbContext;
        }
        public DataAccess.Customer GetById(string id)
        {
            return dbContext.Set<DataAccess.Customer>().FirstOrDefault(x=>x.CustomerID.ToString() == id);
        }
    }
}

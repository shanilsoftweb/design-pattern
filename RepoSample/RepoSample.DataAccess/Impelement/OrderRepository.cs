﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.DataAccess.Impelement
{
    public class OrderRepository : BaseRepository<DataAccess.Order>,Interface.IOrderRepository<DataAccess.Order>
    {
        public OrderRepository ()
            : base()
        {

        }
        public OrderRepository(DbContext dbContext)
            : base(dbContext)
        {
         
        }
        public IEnumerable<Order> GetOrderBycustomerId(string id)
        {
            return base.GetAll().Where(x => x.CustomerID.ToString() == id).ToList();
        }
    }
}

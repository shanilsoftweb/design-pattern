﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.Entity
{
    public class Customer
    {
        public string CustomerID { get; set; }
        
        public string ContactName { get; set; }

        public string CompanyName { get; set; }
    }
}

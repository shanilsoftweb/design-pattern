﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepoSample.Entity
{
    public class Order
    {
        public int OrderID { get; set; }
        public string ShipName { get; set; }
        public Customer Customer { get; set; }
    }
}

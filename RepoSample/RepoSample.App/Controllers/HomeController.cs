﻿using RepoSample.BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RepoSample.App.Controllers
{
    public class HomeController : Controller
    {
        CustomerLogic customer = new CustomerLogic();
        OrderLogic Order = new OrderLogic();
        public ActionResult Index()
        {
            List<Entity.Customer> cusList = customer.GetAll();
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

        //    customer.Insert(new Entity.Customer() { ContactName = "xyx", CompanyName ="Company XYZ"});

            //foreach (var cus in cusList)
            //{
            //    var x= Order.GetByCustomerId(cus.CustomerID);
                
            //}

            return View(cusList);
           
        }
        public PartialViewResult GetOrder()
        {

            var x = Order.GetByCustomerId(Request.QueryString["ddDropDown"]);
            return PartialView("_Order", x); 
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
